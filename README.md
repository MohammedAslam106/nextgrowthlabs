# Task Report: Next Growth Labs

## Overview
This report summarizes the work I have completed for Next Growth Labs as per the assigned task. I have created a video explanation that covers all the details of my work, and you can access it through the following link:

[Video Explanation](https://www.youtube.com/watch?v=KqMnFYV4vpk)

In the video, I provide a comprehensive explanation of the tasks undertaken and the outcomes achieved. Below, I will briefly outline the key points discussed in the video.

## Task Details

### Task 1: [Making a simple website with vanilla JS]

### Task 2: [Lazy Loading To Avoid Pagination (vanilla js)]

## Conclusion

The video linked above provides a detailed overview of the tasks I have completed for Next Growth Labs. Please refer to the video for a comprehensive understanding of my work. If you have any further questions or need additional information, feel free to reach out to me.

[Dekstop Proformance](https://uploadnow.io/files/QNjdPLJ)
[Mobile Proformance](https://uploadnow.io/files/HYw23nV)

Thank you for the opportunity to contribute to Next Growth Labs.

Sincerely,

[Mohammed Aslam]
[mohammedaslam4106@gmail.com]


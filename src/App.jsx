import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import { Routes,Route } from 'react-router-dom' 
import Home from './components/Home'
import Task1 from './components/Task1'
import Task2 from './components/Task2'

function App() {

  return (
    <>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/task-1' element={<Task1/>}/>
        <Route path='/task-2' element={<Task2/>}/>
      </Routes>
    </>
  )
}

export default App

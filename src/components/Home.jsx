import { useNavigate } from "react-router"

export default function Home(){
    const navigate=useNavigate()
    return(
        <>
            <div style={{minHeight:'100vh'}} className=" w-100 d-grid align-items-center justify-content-center">
                <div   className=" d-flex flex-column gap-3 justify-content-center align-items-center">
                    <div onClick={()=>navigate('task-1')} className=" align-content-lg-start p-3 border rounded shadow-sm btn section">
                        <h5>Section-A: Making a simple website with JS</h5>
                        <h6 className=" float-start">
                            There are two tasks under this section.
                        </h6> <br />
                            <p className=" mb-auto w-100 float-start text-justify">
                                Task-1: BootStrap <br />
                                Task-2: JavaScript (JS)
                            </p>
                    </div>
                    <div onClick={()=>navigate('task-2')} className=" align-content-lg-start p-3 border rounded shadow-sm btn section">
                        <h5>Section-B: Lazy Loading To Avoid Pagination</h5>
                            <p className=" mb-auto float-start text-start text-justify">
                            To avoid pagination, Frontend devs came up with the solution of lazy loading, so when a user reaches the end of the page, more results are automatically loaded to have a good user experience.
                            </p>
                    </div>
                </div>
            </div>
        </>
    )
}
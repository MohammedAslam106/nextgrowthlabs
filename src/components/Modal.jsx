/* eslint-disable react/prop-types */
import Modal from 'react-bootstrap/Modal';

function MyModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.heading}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.children}
      </Modal.Body>
      <Modal.Footer>
        <button className=' btn btn-secondary ' onClick={props.onHide}>Close</button>
        <button form="form-1" type='submit' className=' btn btn-primary'>Submit</button>
      </Modal.Footer>
    </Modal>
  );
}

export default MyModal;
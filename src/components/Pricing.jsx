import { useState } from "react";
import PricingFooter from "./PricingFooter";
import PricingHeader from "./PricingHeader";
import PricingNavbar from "./PricingNavbar";
import PricingPlans from "./PricingPlans";


export default function Pricing() {
    const [prefered,setPrefered]=useState(0)
    const [rangerValue,setRangerValue]=useState(0)
  return (
    <div className="App"  id="top">
      <PricingNavbar />
      <PricingHeader/>
      <div className="container">
        <PricingPlans setPrefered={setPrefered} setRangerValue={setRangerValue} prefered={prefered}/>
        <div className=" mx-4">
            <div  className=" border p-2 rounded shadow-sm">
                <input onChange={(e)=>{
                    setRangerValue(e.target.value)
                    if(e.target.value<=25) setPrefered(0)
                    else if(e.target.value>25 && e.target.value<=50) setPrefered(1)
                    else setPrefered(2)
                }} 
                    value={rangerValue} className=" w-100" type="range" min={0} />
            </div>
            <div className=" d-grid justify-content-center my-3 mb-auto">
                <span className=" py-2 px-2 rounded border shadow-sm">{rangerValue}-{rangerValue<=25?25:rangerValue<=50?50:100}</span>
            </div>
            <div className=" d-grid justify-content-center">
                <span className=" text-center">Users Between</span>
            </div>
        </div>
        <PricingFooter />
      </div>
    </div>
  );
}

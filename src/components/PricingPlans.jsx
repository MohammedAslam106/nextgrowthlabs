/* eslint-disable no-unused-vars */
import { useState } from "react";
import MyModal from "./Modal";
import emailjs  from "@emailjs/browser";

/* eslint-disable react/prop-types */
const planContents = [
  {
    header: "Free",
    price: 0,
    features: [
      "10 users included",
      "2 GB of storage",
      "Email support",
      "Help center access"
    ],
    buttonLabel: "Sign up for free",
    outline: true
  },
  {
    header: "Pro",
    price: 15,
    features: [
      "20 users included",
      "10 GB of storage",
      "Priority email support",
      "Help center access"
    ],
    buttonLabel: "Get started",
    outline: false
  },
  {
    header: "Enterprise",
    price: 29,
    features: [
      "30 users included",
      "15 GB storage",
      "Phone and email support",
      "Help center access"
    ],
    buttonLabel: "Contact us",
    outline: false
  }
];

const Plan = props => {
    const[showModal,setShowModal]=useState(false)
    const [name,setName]=useState('')
    const [email,setEmail]=useState('')
    const [comments,setComments]=useState('')
    const formHandler=async(e)=>{
        e.preventDefault()
        console.log(e.currentTarget)
        emailjs.sendForm(import.meta.env.VITE_SERVICE_ID, import.meta.env.VITE_TEMPLATE_ID, e.currentTarget,import.meta.env.VITE_PUBLIC_KEY)
        .then(function(response) {
          console.log('SUCCESS!', response.status, response.text);
          if(response.status) {
            alert('Thanks for showing your interest!')
            setShowModal(false)}
        }, function(error) {
          console.log('FAILED...', error);
        });
    }
  return (
    <>

    <div  className={"card mb-4 shadow-sm " + props.className }>
      <div className={"card-header " + 'active-plan-header'}>
        <h4 className="my-0 font-weight-normal">
          {props.header}
        </h4>
      </div>
      <div className="card-body">
        <h1 className="card-title pricing-card-title">
          {`$${props.price}`}
          <small className="text-muted">
            / mo
          </small>
        </h1>
        <ul className="list-unstyled mt-3 mb-4">
          {props.features.map((feature, i) => (
            <li key={i}>{feature}</li>
          ))}
        </ul>
        <button
            onClick={()=>{
                setShowModal(true)
                // props.header=='Free'?setShowModal0(true):props.header=='Pro'?setShowModal1(true):setShowModal2(true)
            }}
          className={`btn btn-lg btn-block ${
            props.outline
              ? "btn-outline-primary"
              : "btn-primary"
          }`}
          type="button"
        >
          {props.buttonLabel}
        </button>
      </div>
    </div> 
      <MyModal heading={props.header} show={showModal} onHide={()=>setShowModal(false)}>
        <h4 className=" text-center">{props.header=='Free'?"Signup":props.header=='Pro'?'Get Started':'Enterprise'}</h4>
        <form id="form-1" onSubmit={formHandler} >
            <ul className="d-flex flex-column justify-content-center gap-3 list-unstyled">
                <li className=" d-flex flex-column w-100">
                    <label >Name</label>
                    <input required onChange={(e)=>setName(e.target.value)} className=" py-2 px-2 rounded shadow-sm  border-black" type="text" name="from_name" placeholder="Name" />
                </li>
                <li className=" d-flex flex-column">
                    <label >E-mail</label>
                    <input required onChange={(e)=>setEmail(e.target.value)} className=" py-2 px-2 rounded shadow-sm  border-black" type="email" name="reply_to" placeholder="E-mail" />
                </li>
                <li className=" d-flex flex-column">
                    <label >Comments</label>
                    <textarea required onChange={(e)=>setComments(e.target.value)} className=" py-2 px-2 rounded shadow-sm  border-black" type="text" name="message" placeholder="Order comments" />
                </li>
            </ul>
        </form>
       </MyModal>

    </>

  );
};

const PricingPlans = ({prefered,setRangerValue,setPrefered}) => {
  // eslint-disable-next-line no-unused-vars
  const plans = planContents.map((obj, i) => {
    return (
    <div onClick={()=>{
        setRangerValue(i==0?0:i==1?26:51)
        setPrefered(i)
        }} key={obj.header}style={{cursor:'pointer'}} className=" col " >
        <Plan
          className={`${i==prefered && 'active-plan'}`}  
          header={obj.header}
          price={obj.price}
          features={obj.features}
          buttonLabel={obj.buttonLabel}
          outline={obj.outline}
        />
    </div>
    );
  });

  return (
    <div className="row row-cols-1 row-cols-md-3 text-center">
      {plans}
    </div>
  );
};

export default PricingPlans;

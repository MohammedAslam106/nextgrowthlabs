import { useEffect, useState } from "react"

export default function Task2(){
    const [data,setData]=useState(null)
    const [endInd,setEndInd]=useState(6)
    useEffect(()=>{
       const products=async()=>{
            const response=await (await fetch('https://fakestoreapi.com/products')).json()
            console.log(response)
            setData(response)
       }
       products()
    },[])
    // const body=document.getElementsByTagName('body')[0]
    // body.addEventListener('scrollend',()=>{
    //     console.log('here i')
    //     setEndInd(endInd+6)
    // })
    window.onscrollend=()=>{
        setTimeout(()=>{
            setEndInd(endInd+6)

        },1500)
    }
    return(
        <>
            <div className=" container h-auto">
                <h1 className=" text-center p-3">Products Available</h1>
                <div className=" d-flex flex-wrap justify-content-center gap-5">
                    {data?.slice(0,endInd).map((product,ind)=>{
                        return(
                                <div className=" card p-2 " key={ind}>
                                    <img style={{width:'300px', height:'260px'}} loading="lazy" src={product.image} alt="" />
                                    <h6 style={{width:'300px'}} className=" text-center">{product.title}</h6>
                                </div>
                        )
                    }
                    )}
                </div>
                {data?.lenght!=20 && <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}} className=" my-3">
                    <div className="loader"></div>
                    <p>Loading more content</p>
                </div>}
            </div>
        </>
    )
}